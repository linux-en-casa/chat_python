# Analizador de chat para jugar Ahorcado. 


<img src="./ahorcado.gif" alt="chat ahorcado">


## Descripción.
Un simple script que permite captura y filtra 
mensage de un chat live stream para 
dar ordenes en la PC del streamer logrando 
asi, jugar al ahorcado. 

## Intruciones de uso. 
### iniciar el script
inicia el script con 
>`python ./ahorcado.py`

pasar la url del directo para que comenzar la catura del 
chat, la url puede ser pasa con el con le parametro

>`python ./ahorcado.py --url "http://exaplen.url/direto"`

o cambiar la url por defecto en la linea 83, de [ahorcado.py](ahorcado.py#l83), por defecto esta `https://www.twitch.tv/nefasque`


---
### inicial el ahorcado
iniciar el juego de ahorcado en una terminal aparter [ahorcado.sh](./ahorcado.sh).

preguntara por una palabra
```bash
./ahorcado.sh
```

 escogera una palabra aleatoria
```bash 
 ./ahorcado.sh -r
 ```
--- 

### Mesaje de visita
Entonce los usuarios y sus mensajes seran interpretado. solo lo siguiente mensaje permitido para usarios. 

> una unica letras. de la A a la Z <br>
> ` @user: H` <br>
> ` @user: y` <br>
> ` @user: o` <br>

> para ingresar palabras completa es con el simbolo de mayor `>` 
> ante de la palabra. ejemplo. <br>
> `@user: >manzana` <br>
> `@user: >cabaña` <br>
> `@user: >jola` <br>

--- 

### max Repeticion
para disminuir la carga por tantos mesajes el 
script solo ejecutara ordenes cuando esta 
se repitan unas cunasta veces por defecto 3. 
puede cambiar el valor de esto con parametros `--max [int]`<br>
ejemplo :


```bash 
python ./ahorcado.py --max 1
```
en este caso se tiene que repetir una ves.<br>
para que se instantanio la insercion de comandos. agregre 0

```bash
python ./ahorcado.py --max 0
``` 

<br>

> **Nota importante:**
> 
> cotador no aplica al a los comandos de Admin 

<br>

--- 

### command Admin
el usario admin deser ser pasado como parametros 
```bash
python ./ahorcado.py --admin [user]
```

el administrador tiene varios mensaje especiales 
para controlar la situacion. 

> pause el juego <br>
> `admin:#pause`

> banear usario  <br>
> `admin:#basn [user]`


> desbanear usario  <br>
> `admin:#unbasn [user]`

NOTE : en caso de que el parametro `[user]` se pasa 
este en caso de que un `@` como por ejemplo `@nefasque`
este es considerado y para que se pueda usar. sin problema. 

### auto enter. 
por defecto el enter se toma como comdano diferente. y se tendra que dar otro comando para el enter. lo puede cambiar dando el 
parametros 
```bash
--enter True
```

<br>

## about 
> este script fue hecho por [@Nefasque](https://nefasque.github.io/Portal) - [github](https://github.com/Nefasque) - [gitlab](https://gitlab.com/Nefasque)  <br>
> como colaboracion hacia el streamer [@linuxencasa](https://gitlab.com/linux-en-casa) - [youtbe](https://www.youtube.com/@LinuxenCasa) - [twitch](https://www.twitch.tv/linuxencasa)<br>

<br>

## *GNU General Public License v3.0**


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
