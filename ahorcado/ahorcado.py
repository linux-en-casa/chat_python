# Titulo : ahorcado
#
# descripción : un controlador que capture mensajes del chat
#               chat de twitch/youtube y mande el resultado
#               al juego de ahorcado.
#
# autor       : Nefasque
#             : github.com/Nefasque
#             : gitlab.com/Nefasque

import pyautogui
from chat_downloader import ChatDownloader
import re
import argparse

####################
# captura de parámetros
####################
parser = argparse.ArgumentParser(description='Twitch Chat Bot')

# url del chat
parser.add_argument('--url', type=str, help='url of chat')
# máximo de mensajes
parser.add_argument('--max', type=int, help='number of messages to capture')
# enter automático
parser.add_argument('--enter', type=bool, help='automatic enter')
# user admin
parser.add_argument('--admin', type=str, help='user admin')


#######
# Mensaje de ayuda --help
#######
parser.add_argument_group(description="------------------")
parser.add_argument_group(description="""
    Commando de admin
    el admin puede escribir cierto mensaje
    que se mande al juego
    como comando para el script.
""")

parser.add_argument_group(
    description="#pause       detiene/reanuda la toma de mensajes ")
parser.add_argument_group(
    description="#ban [user]  espesifica un usuario para banearlo")
parser.add_argument_group(
    description="#unban [user] espesifica un usuario para desbanearlo")


parser.add_argument_group(description="------------------")
parser.add_argument_group(description="""
    Para los jugadores del chat tiene los siguientes
    mensajes para interactuar con el juego:
""")
parser.add_argument_group(description="""
    [letra] -> escribe unicamente una letra
""")
parser.add_argument_group(description="""
    >[palabra] -> escribe una palabra para tratar de adivinarla.
    el simbolo de '>' es OBLIGATORIO.
""")


#######
# toma de valores por los argumentos
#####
# si se pide un maximo
if isinstance(parser.parse_args().max, int):
    max = parser.parse_args().max
else:
    max = 3

# si se paso la url
if parser.parse_args().url:
    url = parser.parse_args().url
else:
    url = 'https://www.twitch.tv/nefasque'

# si se paso usuario es admin
if parser.parse_args().admin:
    admin = parser.parse_args().admin
else:
    admin = 'nefasque'


#####
# variables
#####

# ultimo tecla dada
lastkey = ""
# contador de comandos repetidos
cont = 0
# si el juego se pauso
is_paused = False
# lista de usuarios banneados
banneados = []

####
# Funciones
####


# presionar la tecla enter si como argumento
# se paso --enter true
def autoEnter():
    if parser.parse_args().enter:
        pyautogui.press("enter")


# elimina linea escritas ("solo para terminales")
def deletePromt():
    pyautogui.hotkey("ctrl", "w")


# escribe una palabra
def writeWord(word):
    deletePromt()
    pyautogui.write(word)
    autoEnter()


# remplaza el prom actual por la tecla
def keypress(key):
    if key != 'enter':
        deletePromt()
    pyautogui.press(key)
    autoEnter()


# cuentas cuantas vece se repite una orden
def contandor_press(key, cmd):
    global lastkey
    global cont
    global max

    cont += 1

    if key != lastkey:
        lastkey = key

    if cont < max:
        return

    if cmd == "word":
        writeWord(key)
    elif cmd == "key":
        keypress(key)

    cont = 0


# bannear usuario molesto
def banUser(user):
    global banneados
    if user[0] == "@":
        user = user[1:]

    if user == admin:
        print("No te banes a ti mismo VACA. ")
        return

    banneados.append(user)
    print('el usuario', user, 'fue baneado')


# comprobar si un fue banneado
def checkBan(user):
    global banneados

    for b in banneados:
        if b == user:
            print('el usuario', user, 'esta baneado')
            return True

    return False


# quitar un usuario del baneo
def unbanUser(user):
    global banneados
    if user[0] == "@":
        user = user[1:]
    try:
        banneados.remove(user)
        print('el usuario', user, 'fue desbaneado')
    except ValueError:
        print('el usuario', user, 'no estaba baneado')


########
# MAIN
#######
chat = ChatDownloader().get_chat(url)
for message in chat:
    chat.print_formatted(message)

    author = message['author']['name'].lower()
    message = message['message'].lower()

    # comandos de administrador
    if message == '#pause' and author == admin:  # pausar juego
        is_paused = not is_paused
        if is_paused:
            print('el juego se pauso')
        else:
            print('continue')

    if re.match(r'^#ban', message) and author == admin:  # bannear user
        message = message.split()[-1]
        banUser(message)

    if re.match(r'^#unban', message) and author == admin:  # desbanear user
        message = message.split()[-1]
        unbanUser(message)

    # si el juego esta pausado no continua
    if is_paused:
        continue

    # comprobar si el usuario del mensaje actual fue baneado
    if checkBan(author):
        continue

    # comprobar si el mensaje es una letra o enter
    if re.match(r'^[a-z]$', message) or message == 'enter':
        contandor_press(message, "key")
        continue

    # comprobar si el mensaje es una palabra
    if re.match(r'^>', message):
        contandor_press(message[1:], "word")
        continue
