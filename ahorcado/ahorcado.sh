#!/bin/bash
 # FUNCIONA PERFECTAMENTE
 # Autores: Jorge Blanco De Gracia & Pablo Morata López &  Daniel G. Trabada
 # Licencia : GPL v3
'
     _____
    |   \|
    |    |
    |    |
         |
         |
 ________|____
  
  
    _____
    |   \| ALERTA.
    |    | HAS COMETIDO UN ERROR.
   O|O   | YA NO TIENES CABEZA.
         | 
         | 
 ________|____
  
     
    _____
    |   \| ALERTA 2.
    |    | HAS COMETIDO DOS ERRORES.
   O|O   | YA NO TIENES BRAZO.
  //     | 
         |
 ________|____
  
  
    _____
    |   \| ALERTA 3. 
    |    | HAS COMETIDO TRES ERRORES. 
   O|O   | TE HAS QUEDADO SIN DOS BRAZOS.
  // \\  | 
         |
 ________|____
  
  
    _____
    |   \| ALERTA 4. 
    |    | HAS COMETIDO CUATRO ERRORES. 
   O|O   | SOLO TE QUEDA UNA PIERNA. 
  // \\  | SI FALLAS UNA VEZ MAS, PIERDES.
  ||     | 
 ________|____
  
 BUSCALO EN GOOGLE. SI LO ENCUENTRAS, AVISA.
     _____
    |   \| ERES #### DEL ####.
    |    | YA NO HAY ALERTAS. HAS PERDIDO.
   O|O   | 
  // \\  |
  || ||  | 
 ________|____
 '
  
if (( $1 == "-r" )); then
  word=$(cat ./word.txt)
  word=(${word[@]})
  index=$((1 + RANDOM % ${#word[@]}))
  word=${word[index]}
else 
  clear
  echo -n "¿QUE PALABRA VAMOS A ADIVINAR?: "
  read word
  echo
fi

# echo "Pulsa enter para continuar..."
# read continuar
 letra="*"  
  
 fallos=5 
  
 letras=`echo "$word" | sed "s/[^${letra}]/*"/g` 
  
 sust=$letras
  
#MIENTRAS TODOS LOS CARACTERES NO SEAN IGUALES A LA PALABRA...
 while [ "$sust" != "$word" ]; do
      clear
      echo
      echo "LAS LETRAS SON $sust"
      echo
      sed -n "$fallos,$((fallos+7))p" "$0"
      echo
      echo LETRAS INTRODUCIDAS HASTA AHORA: "$a"
      echo 
      echo -n "INTRODUCE UNA LETRA: "
          read letra
      a=${a}$letra
      palabra=${letra}$palabra    #CONCATENA LAS LETRAS QUE METEMOS CON LAS QUE HAY YA INTRODUCIDAS 
      echo
      existe=`echo "$word" | grep "$letra"` #COMPARA, Y SI NO SON IGUALES, NO MUESTRA NADA

      if [ "$existe" = "" ]; then
        echo "OOOOOH NO ES CORRECTO. BUSCALO EN GOOGLE - ${letra} - NO APARECE EN LA PALABRA OCULTA"
        error=$((error + 1))
        if [ "$error" = 5 ]; then
          clear
          fallos=$(($fallos+9))
          sed -n "$fallos,$((fallos+7))p" "$0"
          echo
          echo -e "\e[33mHAS COMETIDO $error ERRORES. ESTAS MUERTO\e[0m"
          echo "LA PALABRA SECRETA ES: ${word}"
          echo
          exit
        else
          if [ "$error" = 1 ]; then
            echo "HAS COMETIDO $error ERROR"
            fallos=$(($fallos+9))
           else
            fallos=$(($fallos+9))      
          fi
        fi
      else
        sust=`echo "$word" | sed "s/[^${palabra}]/*"/g` #SUSTITUYE LAS LETRAS POR LOS * ASTERISCOS
      fi
      if [ "$sust" = "$word" ]; then
          clear
          echo
          echo -e "ENHORABUENA. HAS DESCUBIERTO LA PALABRA SECRETA. LA MISMA ES: \e[32m${word}\e[0m."
          echo
          echo "  ::        ::   " 
          echo "  ::        ::   "
          echo "        P        "
          echo "    _      _     "
          echo "     \____/      "
          echo
          echo
          echo
      fi
 done
