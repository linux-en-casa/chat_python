#  Programa para manejar los juegos sudoku y sokoban
#  usando mensajes desde el chat de twitch

#  nota:  las combinaciones de teclas funcionan
#  en bspwm  con los juegos en el Escritorio 6
#  sokoban a la izquierda  y sudoku a la derecha
#  no he probado en ningun otro WM

###################################################
#  librerías y funciones que necesito
###################################################
#  para leer el chat de twitch
from chat_downloader import ChatDownloader
#  para enviar combinaciones de teclas al sistema
import pyautogui

#  utilizo pygame para el sonido
#  archivos en wav paso-01 y paso-03
import pygame

##################################################
#  empiezo definiendo los sonidos del juego
pygame.init()
pygame.mixer.init()
#  según una página los .wav dan menos problemas
sonido = pygame.mixer.Sound("paso-01.wav")
sonido_sokoban = pygame.mixer.Sound("paso-01.wav")
sonido_sudoku = pygame.mixer.Sound("paso-03.wav")
##################################################

#  condiciones iniciales del administrador y los jugadores
# el administrador podrá modificar el usuario que juega
administrador = "linuxencasa"
cambiar_administrador = False

# usuarios que juegan
# el nombre debe ser igual al nombre de usuario de twitch
user_sokoban = "linuxencasa"
cambiar_sokoban = False

user_sudoku = "linuxencasa"
cambiar_sudoku = False

# inicialmente yo podría jugar ambos juegos
# y durante el juego puedo asignar otros jugadores
# enviando 2 mensajes por twitch
# también podría modificar el administrador

letras_sokoban = "asdw"
letras_sudoku = "hjkl0123456789"
# un simple string, luego con find y len filtro cualquiera de esas
# 4 letras y numeros

#  creo un diccionario con los 4 movimientos de sokoban
mov_sokoban = {
    'w' : "up",
    's' : "down",
    'a' : "left",
    'd' : "right"
}
#  buscaría el movimiento según la letra que se haya pulsado

#  también creo un diccionario con los 4 movimientos de sudoku
#  podría ser un solo diccionario, pero lo dejo así por claridad
mov_sudoku = {
    'k' : "up",
    'j' : "down",
    'h' : "left",
    'l' : "right"
}
#  buscaría el movimiento según la letra que se haya pulsado

#################################################################
###  funciones    funciones   funciones
#################################################################
#   para enviar comandos de sokoban y algunos de sudoku
def enviar_letra(ventana, letra):
    pyautogui.hotkey('win', '6')
    pyautogui.hotkey('win', ventana)
    pyautogui.press(letra)

#  para enviar combinaciones en sudoku
def enviar_combinacion(ventana, mod, letra):
    pyautogui.hotkey('win', '6')
    pyautogui.hotkey('win', ventana)
    pyautogui.hotkey(mod, letra)


###################################################
##################################################
#  aquí empezaría el programa

#  lo primero es leer los mensajes del chat

url = 'https://www.twitch.tv/linuxencasa'
chat = ChatDownloader().get_chat(url)       # create a generator
for message in chat:                        # iterate over messages
    chat.print_formatted(message)           # print the formatted message
    print(message['author']['name'])

# imprimo todos los mensajes solo para llevar un registro
# también me sirve para ver los nombres de los usuarios

#     estos dan demasiada información:
#     solo lo usé cuando estaba analizando la info del chat
#     el chat de YT es un poco diferente
#     print(message)
#     print(message['author'])
#     print(' el siguiente es el display name')
#     print(message['author']['display_name'])
#     print(' y ahora solo el name')


###################################################
#  sokoban   sokoban   sokoban   sokoban   sokoban
#  estaría en el tablero de la izquierda

#  movimientos de sokoban
    if message['author']['name'].lower() == user_sokoban:
        mensaje_minuscula = message['message'].lower()
        if len(mensaje_minuscula) == 1:
            if letras_sokoban.find(mensaje_minuscula) != -1:
                enviar_letra("left", (mov_sokoban[mensaje_minuscula]))
                pygame.mixer.Sound.play(sonido_sokoban)

#  ingresar o salir de sokoban
#  enviando enter o esc
#  que son exactamente las mismas palabras que necesita pyautogui
        elif (mensaje_minuscula == 'enter') | (mensaje_minuscula == 'esc'):
            enviar_letra("left", mensaje_minuscula)
            pygame.mixer.Sound.play(sonido_sokoban)

#  sokoban solo necesita las letras a, s, d, w para moverse
#  y enviar enter  y esc  para aceptar y salir

#########################################################
#  hasta aquí iría sokoban

#########################################################
#   sudoku     sudoku     sudoku     sudoku
#########################################################
#  en el tablero de la derecha
#  y con las teclas h, j, k, l

    if message['author']['name'].lower() == user_sudoku:
        mensaje_minuscula = message['message'].lower()

        if len(mensaje_minuscula) == 1:   #  un solo caracter
            if letras_sudoku.find(mensaje_minuscula) != -1:   #  y está en letras_sudoku
                if mensaje_minuscula.isnumeric() == True:
                    mensaje_minuscula = f"+{mensaje_minuscula}"
                else:
                    mensaje_minuscula = f"1{mensaje_minuscula}"

                #  si detecta un número le agrego un simbolo +
                #  para que el mensaje_minuscula quede con 2 caracteres
                #  e ingrese al if de len == 2
                #  si no es un número solo quedan las letras hjkl
                #  es decir moverme por el tablero
                #  le agrego el numero 1 para ingrese al proximo if
                #  y mueva 1 posición

        #  movernos por el tablero 1, 2, 3, 4...9 pasos
        #  en la forma   4j     2l   7k
        #  primero el número seguido de una letra
        if len(mensaje_minuscula) == 2:
            if mensaje_minuscula[0].isnumeric() == True:
                if letras_sudoku.find(mensaje_minuscula[1]) != -1:
                    pyautogui.hotkey('win', '6')       #  de nuevo esto solo aplica a bspmw
                    pyautogui.hotkey('win', 'right')   #  en escritorio 6, ventana de la derecha
                    pygame.mixer.Sound.play(sonido_sudoku)
                    print(mensaje_minuscula)
                    movimientos = int(mensaje_minuscula[0])
                    while movimientos>0:
                        movimientos-=1
                        pyautogui.press(mov_sudoku[mensaje_minuscula[1]])

            #   y el siguiente if es para escribir o borrar los números en el tablero
            #   los números normales son de la forma  +4 +5
            #   los numeros pequeños de la forma -3 -5
            #   se borra con el 0
            #   también tendría 2 letras así que seguimos en el mismo if con len = 2
            if mensaje_minuscula[0] == "-":
                if mensaje_minuscula[1].isnumeric() == True:
                    pygame.mixer.Sound.play(sonido_sudoku)
                    enviar_combinacion("right", "ctrl", mensaje_minuscula[1])

            if mensaje_minuscula[0] == "+":
                if mensaje_minuscula[1].isnumeric() == True:
                    pygame.mixer.Sound.play(sonido)
                    enviar_letra("right", mensaje_minuscula[1])


#############################
#############################
#  y estos serían los comandos para ingresar o salir de sudoku:
#############################

        if message['message'].lower() == 'intro':
            print('sudoku enter')
            enviar_letra("right", "enter")
            pygame.mixer.Sound.play(sonido)


        elif message['message'].lower() == 'ctrl n':
            print('sudoku nuevo')
            enviar_combinacion("right", "ctrl", "n")
            pyautogui.press('down')
            pygame.mixer.Sound.play(sonido)


        elif message['message'].lower() == 'ctrl r':
            print('sudoku reiniciar')
            enviar_combinacion("right", "ctrl", "r")
            pyautogui.press('right')
            pyautogui.press('enter')
            pygame.mixer.Sound.play(sonido)


#  modificar los jugadores enviando dos mensajes en el chat de twitch
#  primero envio un mensaje diciendo "nuevo jugador de sokoban"
#  o "nuevo jugador de sudoku"
#  y el siguiente mensaje es el nuevo jugador

    if message['author']['name'].lower() == administrador:
        if message['message'].lower() == 'nuevo jugador de sokoban':
            cambiar_sokoban = True
        elif cambiar_sokoban == True:
            user_sokoban = message['message'].lower()
            cambiar_sokoban = False

        if message['message'].lower() == 'nuevo jugador de sudoku':
            cambiar_sudoku = True
        elif cambiar_sudoku == True:
            user_sudoku = message['message'].lower()
            cambiar_sudoku = False


#  y esto para cambiar el administrador
#  igual con 2 mensajes - aunque no lo he probado
#  pues si me equivoco tendría que reiniciar el juego
        if message['message'].lower() == 'nuevo administrador':
            cambiar_administrador = True
        elif cambiar_administrador == True:
            administrador = message['message'].lower()
            cambiar_administrador = False


##############################################################
##############################################################
#   hasta aquí vamos - todo lo demás son pruebas o está pendiente
##############################################################
##############################################################



#  otra opcion para borrar los números
#  funciona en el sudoku por linea de comandos

#        elif message['message'].lower() == 'x':
#            pyautogui.hotkey('win', 'right')
#            print('borrar número :)')
#            pyautogui.press('space')
#            sonido_sudoku()

