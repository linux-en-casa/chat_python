# Proyecto Chat de Twitch con Python

Estoy aprendiendo python y he creado un programa para controlar 2 juegos sencillos a través de mensajes del chat de twitch.  Los juegos son Sokoban y Sudoku.

En python utilizo las librerías:
* chat-downloader  -  para leer los mensajes del chat
* pyautogui  -  para enviar combinaciones de teclas o comandos a los juegos
* pygame  -  para reproducir algunos sonidos .wav

## Configuración inicial
Para comenzar se deben abrir los juegos Sokoban y Sudoku en el Escritorio 6; sokoban al lado izquierdo y sudoku al lado derecho.  Esta primera prueba la hice en Linux Debian con el Gestor de Ventanas BSPWM y las combinaciones de teclas solo las he probado en ese Gestor de Ventanas y solo en el Escritorio 6 con esa disposición.

En otro Escritorio se debe lanzar o ejecutar el archivo soloban-sudoku.py desde un ambiente python, el programa va a quedar a la espera de los mensajes del chat de Twitch.
Y también ya debe estar activo el Directo de Twitch desde donde se va a leer el chat.


El juego se muestra en este [directo de YouTube](https://youtube.com/live/wp8buprmeUM)


##  Cómo se juega

### Sokoban
Para movernos por el tablero se usan las teclas a, s, d, w:
* a - Izquierda - left
* s - Abajo - down
* d - Derecha - right
* w - Arriba - up

Se debe enviar una sola letra a la vez.

Otros comandos
* Salir o reiniciar el juego: Se debe enviar el mensaje esc
* Para aceptar se debe enviar la palaba enter


### Sudoku
Para movernos por el tablero se usan las teclas h, j, k, l:
* h - Izquierda - left
* j - Abajo - down
* k - Arriba - up
* l - Derecha - right

De nuevo se debe enviar una sola letra a la vez.  Aunque también es posible enviar una combinación de un número y letra, así:
4j
3k
2l
9h

para desplazarnos varias posiciones a la vez.  Un número del 1 al 9 seguido de una letra.

Para escribir los números en el tablero se debe enviar un mensaje con un solo número.  Para borrar se usa el 0.

En el tablero de sudoku también es posible enviar o escribir números pequeños que sirven para dejar números tentativos o posibles.  Esto se hace enviando los números anteponiendo el signo menos, es decir
-4
-5
-8

Otras combinaciones o comandos:
Para limpiar el tablero  se envia un mensaje con:
ctrl r

Para salir del juego se envia un mensaje con:
ctrl n

Para aceptar se envia la palabra
intro


## Nota 1
Dejé las letras y combinaciones de teclas diferentes para cada juego y así una sola persona podría jugar los dos juegos a la vez.

## Jugadores.
Inicialmente está configurado que el usuario "linuxencasa" pueda jugar los 2 juegos y también es el administrador.  Pero durante el directo se puede cambiar los jugadores.  Para cambiar los jugadores, el administrador debe enviar 2 mensajes así:

Primer mensaje:  nuevo jugador de sokoban
Segundo mensaje:  debe enviar el nombre exacto del jugador - que se obtiene leyendo los mensajes con chat_downloader

Y para cmabiar el jugador de sudoku:
Primer mensaje:  nuevo jugador de sudoku
Segundo mensaje:  el nuevo jugador de sudoku

También se podría cambiar el nombre del administrador cuya única función es asignar los nombres de los jugadores.
Primer mensaje:  nuevo administrador
Segundo mensaje:  el nombre del nuevo administrador


## Nota 2
El chat de twitch tiene configurado un retraso de unos 30 segundos si alguien intenta enviar muchos mensajes repetidos y la única forma de quitar esta limitación es que los jugadores sean moderadores o vip.

/mod jugador1

/mod jugador2

/vip jugador1

/vip jugador2

Para ver el listado de vips y/o moderadores
/vips

/mods

---

> ### Otros juegos 
> 
> [Ahorcado](./ahorcado/README.md) por Nefasque

---

**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.



